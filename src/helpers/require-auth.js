import React, {useState, useEffect} from 'react';
import {Redirect} from 'react-router-dom';
import {config} from '../config';
import axios from 'axios';
import {loadUserInfo} from '../actions/user/userAction';
import {loadRdv} from '../actions/rdv/rdvAction';
import {loadProspect} from '../actions/prospect/prospectAction';
import {getAllRdv} from '../api/rdv';
import {getAllProspect} from '../api/prospect';
import {getAllFollow} from '../api/follow';
import {loadFollow} from '../actions/follow/followAction';
import {connect} from 'react-redux';

export default function (ChildComponent, withAuth=false){
	const RequireAuth = (props)=>{
		const [redirect, setRedirect] = useState(false);

		useEffect(()=>{
			console.log(props.user)
            //récupération du token dans le localstorage
  
            //si le token est null et que withAuth est true
        
                //on envoi la redirection
  
            //sinon
        
                //si l'utilisateur n'est pas connecté
               
                    //requète ajax de vérification du token
                    
                    //.then

                        //si le status de la reponse n'est pas 200
                       
                            //si withAuth est true
                        
                                //on redirige
                        
                            
                        //sinon
                    
                            //appel de l'action du chargement des infos de l'utilisateur
                            
                            //appel des fonctions de recup de tous les rdv, tous les clients, tous les suivis
                            //pour chacun on recup les actions loadRdv, loadProspect et loadFollow
                    
        }, [])

		if(redirect) {
            return <Redirect to="/login" />
        }
        return (<ChildComponent {...props} />)
	}

	const mapDispatchToProps = {
        loadUserInfo,
        loadRdv,
        loadProspect,
        loadFollow
    }
    
    const mapStateToProps = (store)=>{
        return {
            user: store.user
        }
    }
    
    
    return connect(mapStateToProps, mapDispatchToProps)(RequireAuth);
}