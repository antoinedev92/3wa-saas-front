import logo from './logo.svg';
import './App.css';
import Header from './containers/header';
import Home from './containers/home';


import RequireAuth from './helpers/require-auth';
import {Switch, Route} from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Header />
      <Switch>
        <Route exact path="/" component={home} />
      </Switch>
    </div>
  );
}

export default App;